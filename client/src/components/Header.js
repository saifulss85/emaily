import PropTypes from 'prop-types';
import React, { Component } from "react";
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Payments } from './Payments';

class BaseHeader extends Component {
  renderContent() {
    switch (this.props.auth) {
      case null:
        return;
      case false:
        return <li><a href="/auth/google">Sign in with Google</a></li>;
      default:
        return [
          <li key={1}><Payments amount={1} /></li>,
          <li key={2} style={{ margin: '0 10px' }}>Credits: {this.props.auth.credits}</li>,
          <li key={3}><a href="/api/logout">Logout</a></li>,
        ];
    }
  }

  render() {
    return (
      <nav>
        <div className="nav-wrapper">
          <Link to={this.props.auth ? '/surveys' : '/'} className="brand-logo">
            Emaily
          </Link>
          <ul id="nav-mobile" className="right hide-on-med-and-down">
            {this.renderContent()}
          </ul>
        </div>
      </nav>
    );
  }
}

BaseHeader.propTypes = {
  auth: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
};

function mapStateToProps({ auth }) {
  return { auth }
}

export const Header = connect(mapStateToProps)(BaseHeader);