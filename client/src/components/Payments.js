import PropTypes from 'prop-types';
import React, { Component } from "react";
import { connect } from 'react-redux';
import StripeCheckout from 'react-stripe-checkout';
import * as actions from '../actions';

class BasePayments extends Component {
  render() {
    return (
      <div>
        <StripeCheckout
          name="Emaily"
          description="$5 for 5 email credits"
          amount={this.props.amount}
          token={token => this.props.handleToken(token)}
          stripeKey={process.env.REACT_APP_STRIPE_KEY}
        >
          <button className='btn waves-effect waves-light'>Add Credits</button>
        </StripeCheckout>
      </div>
    );
  }
}

BasePayments.propTypes = {
  amount: PropTypes.number.isRequired,
  handleToken: PropTypes.func.isRequired,
};

export const Payments = connect(null, actions)(BasePayments);