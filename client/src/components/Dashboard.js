import * as PropTypes from 'prop-types';
import React from "react";
import { Link } from 'react-router-dom';

export function Dashboard(props) {
  return (
    <div className="fixed-action-btn">
      <Link to="/surveys/new" className="btn-floating btn-large waves-effect waves-light red">
        <i className="material-icons">add</i>
      </Link>
    </div>
  );
}

Dashboard.propTypes = {
  nextCard: PropTypes.func
};