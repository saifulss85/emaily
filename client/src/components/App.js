import PropTypes from 'prop-types';
import React, { Component } from "react";
import { connect } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';
import * as actions from '../actions';
import { Dashboard } from './Dashboard';
import { Header } from './Header';
import { Landing } from './Landing';
import { SurveyNew } from './surveys/SurveyNew';

class BaseApp extends Component {
  componentDidMount() {
    this.props.fetchUser();
  }

  render() {
    return (
      <BrowserRouter>
        <div className="container">
          <Header />
          <Route path="/" exact component={Landing} />
          <Route path="/surveys" exact component={Dashboard} />
          <Route path="/surveys/new" component={SurveyNew} />
        </div>
      </BrowserRouter>
    );
  }
}

BaseApp.propTypes = {
  fetchUser: PropTypes.func.isRequired
};

export const App = connect(null, actions)(BaseApp);