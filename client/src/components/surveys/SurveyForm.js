import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form';
import { validateEmails } from '../../utils/validateEmails';
import { SurveyField } from './SurveyField';

const FIELDS = [
  { label: 'Survey Title', name: 'title' },
  { label: 'Subject Line', name: 'subject' },
  { label: 'Email Body', name: 'body' },
  { label: 'Recipient List', name: 'emails' },
];

class BaseSurveyForm extends Component {
  static renderFields() {
    return FIELDS.map(field => (
      <Field
        label={field.label}
        name={field.name}
        type="text"
        key={field.name}
        component={SurveyField}
      />
    ));
  }

  render() {
    return (
      <div>
        <form onSubmit={this.props.handleSubmit(values => console.log(values))}>
          {BaseSurveyForm.renderFields()}

          <Link to="/surveys" className="grey btn-flat left white-text">
            Cancel
          </Link>

          <button type="submit" className="teal btn-flat right white-text">
            Next
          </button>
        </form>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  errors.emails = validateEmails(values.emails || '');

  FIELDS.forEach(({ name }) => {
    if (!values[name]) {
      errors[name] = `You must provide a ${name}`;
    }
  });

  return errors;
}

export const SurveyForm = reduxForm({
  validate,
  form: 'surveyForm'
})(BaseSurveyForm);

