const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20');
const mongoose = require('mongoose');
const googleClientSecret = require('../config/keys').googleClientSecret;
const googleClientID = require('../config/keys').googleClientID;

const User = mongoose.model('users');

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findById(id)
    .then(user => done(null, user));
});

passport.use(
  new GoogleStrategy(
    {
      clientID: googleClientID,
      clientSecret: googleClientSecret,
      callbackURL: '/auth/google/callback',
    }, createUserIfNotExists)
);

function createUserIfNotExists(accessToken, refreshToken, profile, done) {
  User.findOne({ googleId: profile.id })
    .then(existingUser => {
      if (existingUser) {
        console.log('user already exists', existingUser);
        done(null, existingUser);
      } else {
        new User({ googleId: profile.id, })
          .save()
          .then(newlyCreatedUser => done(null, newlyCreatedUser));
      }
    });
}