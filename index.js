const express = require('express');
const mongoose = require('mongoose');
const cookieSession = require('cookie-session');
const passport = require('passport');
require('./models/User');
require('./models/Survey');
require('./services/passport');
const mongoURI = require('./config/keys').mongoURI;
const cookieKey = require('./config/keys').cookieKey;
const bodyParser = require('body-parser');
const stripePublishableKey = require('./config/prod').stripePublishableKey;

mongoose.connect(mongoURI, { useNewUrlParser: true });

const app = express();

app.use(bodyParser.json());
app.use(
  cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,  // 30 days
    keys: [cookieKey],
  })
);
app.use(passport.initialize());
app.use(passport.session());

app.get('/booyah', (req, res) => {
  console.log('logging to console...');
  console.log('stripePublishableKey', stripePublishableKey);
  console.log('process.env.REACT_APP_STRIPE_KEY', process.env.REACT_APP_STRIPE_KEY);
  res.send('booyah!');
});
require('./routes/authRoutes')(app);
require('./routes/billingRoutes')(app);
require('./routes/surveyRoutes')(app);

if (process.env.NODE_ENV === 'production') {
  // tell Express to serve statics out of client/build folder
  app.use(express.static('client/build'));

  // now that Express is serving statics out of client/build,
  // tell Express to send all requests to unknown paths to serve it to index.html
  const path = require('path');
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });

}

app.get('/', (req, res) => {
  res.send('hello world');
});

const PORT = process.env.PORT || 5000;
app.listen(PORT);